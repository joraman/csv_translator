#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
import argparse
import csv
import json

def main(argv):
	parser = argparse.ArgumentParser()

	parser.add_argument('-c','--csvf',help='CSV entrada', required=True)
	parser.add_argument('-cfg','--config',help='Fitxer de configuració per a la "traducció de CSVs"',required=True)
	parser.add_argument('-o','--output',help='Fitxer de sortida', required=True)

	args = parser.parse_args()

	csvf = args.csvf.rstrip()
	config = args.config.rstrip()
	output = args.output.rstrip()

	config_aux = open(config) #Obrim fitxer de configuració
	config_dict = json.load(config_aux) #Parsejem les dades i les posem en un diccionari que farem servir per a "traduïr" el CSV d'entrada

	with open(csvf) as fitxerEntrada:
		lector = csv.DictReader(fitxerEntrada) 

		with open('./output/'+output, mode='w') as csv_sortida:
			header_output = []

			for iterador in range(1,len(config_dict['order'])+1): #Construir capçalera del fitxer de sortida
				header_output.append(config_dict['order'][str(iterador)])

			writer = csv.DictWriter(csv_sortida, fieldnames=header_output)
			writer.writeheader()


			for linia in lector: #iterem el fitxer d'entrada linia a linia
				row_output = {} 

				for element_header in header_output:
					row_output[element_header] = linia[config_dict['rosetta_stone'][element_header]]

				writer.writerow(row_output) #Escrivim linia al fitxer de sortida


if __name__ == '__main__':
   main(sys.argv[1:])